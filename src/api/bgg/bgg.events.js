/**
 * BggData model events
 */


import { EventEmitter } from 'events';
import BggData from './bgg.model';

const BggDataEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
BggDataEvents.setMaxListeners(0);

// Model events
const events = {
  save: 'save',
  remove: 'remove',
};

function emitEvent(event) {
  return function (doc) {
    BggDataEvents.emit(`${event}:${doc._id}`, doc);
    BggDataEvents.emit(event, doc);
  };
}

// Register the event emitter to the model events
Object.keys(events).forEach(e => BggData.schema.post(e, emitEvent(events[e])));

export default BggDataEvents;
