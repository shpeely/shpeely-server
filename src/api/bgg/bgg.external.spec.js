import bggData from './__bgg_fixtures.json';
import { createModel } from './bgg.external';

describe('BGG External API:', function () {

  it('should create models from the BGG data without an error', () => {
    const result = bggData.map(createModel);
    result.forEach(r => expect(r).not.to.be.undefined);
  });
});
