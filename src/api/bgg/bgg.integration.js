import request from 'supertest';
import app from '../..';

const nipponBggId = 154809;

let newBggData;
let bggLoadedData;

describe('BggData API:', function () {
  this.timeout(60 * 1000);

  describe('GET /api/bgg', () => {
    let bggs;

    beforeEach((done) => {
      request(app)
        .get('/api/bgg')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          bggs = res.body;
          done();
        });
    });

    it('should respond with JSON array', () => {
      expect(bggs).to.be.instanceOf(Array);
    });
  });

  describe('POST /api/bgg', () => {
    beforeEach((done) => {
      request(app)
        .post('/api/bgg')
        .send({
          bggid: 1234,
          weight: 3.3,
          maxPlayers: 7.7,
          name: 'Testgame',
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          newBggData = res.body;
          done();
        });
    });

    it('should respond with the newly created bgg', () => {
      expect(newBggData.name).to.equal('Testgame');
      expect(newBggData.weight).to.equal(3.3);
      expect(newBggData.maxPlayers).to.equal(7.7);
      expect(newBggData.bggid).to.equal(1234);
    });
  });


  describe('GET /api/bgg/query', () => {

    it('should respond with JSON array containing the data that matches the query', (done) => {
      request(app)
        .get(`/api/bgg/query`)
        .query({ name: newBggData.name })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          const bgg = res.body;

          expect(bgg).to.length(1);
          expect(bgg[0].name).to.equal(newBggData.name);

          done();
        });
    });

    it('should respond with an empty array if no data was found matching the query', (done) => {
      request(app)
        .get(`/api/bgg/query`)
        .query({ name: 'non existing name' })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          const bgg = res.body;

          expect(bgg).to.be.instanceOf(Array).and.have.lengthOf(0);

          done();
        });
    });
  });

  describe('GET /api/bgg/:id', () => {

    it('should respond with the requested bgg (with mongo id)', (done) => {
      request(app)
        .get(`/api/bgg/${newBggData._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          expect(res.body.name).to.equal('Testgame');
          expect(res.body.weight).to.equal(3.3);
          expect(res.body.maxPlayers).to.equal(7.7);
          expect(res.body.bggid).to.equal(1234);

          done();
        });
    });

    it('should respond with the requested bgg (with bgg id)', (done) => {
      request(app)
        .get(`/api/bgg/${newBggData.bggid}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          expect(res.body.name).to.equal('Testgame');
          expect(res.body.weight).to.equal(3.3);
          expect(res.body.maxPlayers).to.equal(7.7);
          expect(res.body.bggid).to.equal(1234);

          done();
        });
    });
  });

  describe('PUT /api/bgg/:id', () => {
    let updatedBggData;

    beforeEach((done) => {
      request(app)
        .put(`/api/bgg/${newBggData._id}`)
        .send({
          name: 'Updated Game Name',
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          updatedBggData = res.body;
          done();
        });
    });

    afterEach(() => {
      updatedBggData = {};
    });

    it('should respond with the updated bgg on a subsequent GET', (done) => {
      request(app)
        .get(`/api/bgg/${newBggData._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          const bgg = res.body;

          expect(bgg.name).to.equal('Updated Game Name');

          done();
        });
    });
  });

  describe('PUT /api/bgg/sync/:id', () => {

    after(() => {
      bggLoadedData = {};
    });

    it('should get data from bgg and store it', (done) => {
      request(app)
        .put(`/api/bgg/sync/${nipponBggId}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          bggLoadedData = res.body;
          expect(bggLoadedData.bggid).to.equal(nipponBggId);
          expect(bggLoadedData.name).to.equal('Nippon');
          expect(bggLoadedData.weight).to.be.above(0).and.to.be.below(5);
          done();
        });
    });

    it('should update the data stored in the DB with new data from BGG', (done) => {
      const lastUpdate = bggLoadedData.updatedAt;

      request(app)
        .put(`/api/bgg/sync/154809`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          bggLoadedData = res.body;
          expect(bggLoadedData.lastUpdate).not.to.equal(lastUpdate);

          done();
        });
    });

  });

  describe('DELETE /api/bgg/:id', () => {
    it('should respond with 204 on successful removal (deleting with objectid)', (done) => {
      request(app)
        .delete(`/api/bgg/${newBggData._id}`)
        .expect(204)
        .end((err) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 204 on successful removal (deleting with bggid)', (done) => {
      request(app)
        .delete(`/api/bgg/${nipponBggId}`)
        .expect(204)
        .end((err) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when bgg does not exist', (done) => {
      request(app)
        .delete(`/api/bgg/${newBggData._id}`)
        .expect(404)
        .end((err) => {
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });

  describe('GET /api/bgg/search', () => {

    it('should respond with JSON array', (done) => {
      request(app)
        .get('/api/bgg/search')
        .query({ query: 'agricola' })
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          expect(res.body.length).to.be.above(0);
          done();
        });
    });
  });
});
