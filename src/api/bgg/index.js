

const express = require('express');
const controller = require('./bgg.controller');

const router = express.Router();

router.get('/', controller.index);
router.get('/query', controller.query);
router.get('/search', controller.search);
router.get('/:id', controller.show);
router.post('/', controller.create);
router.put('/:id', controller.upsert);
router.put('/sync/:id', controller.sync);
router.delete('/:id', controller.destroy);

module.exports = router;
