

const proxyquire = require('proxyquire').noPreserveCache();

const bggCtrlStub = {
  index: 'bggCtrl.index',
  show: 'bggCtrl.show',
  create: 'bggCtrl.create',
  upsert: 'bggCtrl.upsert',
  sync: 'bggCtrl.sync',
  destroy: 'bggCtrl.destroy',
  query: 'bggCtrl.query',
};

const routerStub = {
  get: sinon.spy(),
  put: sinon.spy(),
  patch: sinon.spy(),
  post: sinon.spy(),
  delete: sinon.spy(),
  query: sinon.spy(),
};

// require the index with our stubbed out modules
const bggIndex = proxyquire('./index.js', {
  express: {
    Router() {
      return routerStub;
    },
  },
  './bgg.controller': bggCtrlStub,
});

describe('bgg API Router:', () => {
  it('should return an express router instance', () => {
    expect(bggIndex).to.equal(routerStub);
  });

  describe('GET /api/bgg', () => {
    it('should route to bgg.controller.index', () => {
      expect(routerStub.get.withArgs('/', 'bggCtrl.index')).to.have.been.calledOnce;
    });
  });

  describe('GET /api/bgg/:id', () => {
    it('should route to bgg.controller.show', () => {
      expect(routerStub.get
        .withArgs('/:id', 'bggCtrl.show'),
      ).to.have.been.calledOnce;
    });
  });

  describe('GET /api/bgg/query', () => {
    it('should route to bgg.controller.query', () => {
      expect(routerStub.get
        .withArgs('/query', 'bggCtrl.query'),
      ).to.have.been.calledOnce;
    });
  });

  describe('POST /api/bgg', () => {
    it('should route to bgg.controller.create', () => {
      expect(routerStub.post
        .withArgs('/', 'bggCtrl.create'),
        ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/bgg/:id', () => {
    it('should route to bgg.controller.upsert', () => {
      expect(routerStub.put
        .withArgs('/:id', 'bggCtrl.upsert'),
      ).to.have.been.calledOnce;
    });
  });

  describe('PUT /api/bgg/sync/:id', () => {
    it('should route to bgg.controller.upsert', () => {
      expect(routerStub.put
        .withArgs('/sync/:id', 'bggCtrl.sync'),
      ).to.have.been.calledOnce;
    });
  });

  describe('DELETE /api/bgg/:id', () => {
    it('should route to bgg.controller.destroy', () => {
      expect(routerStub.delete
        .withArgs('/:id', 'bggCtrl.destroy'),
        ).to.have.been.calledOnce;
    });
  });
});
