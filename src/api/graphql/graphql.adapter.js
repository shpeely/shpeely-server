
export function transformGameResult (requestBody) {
  const gameresult = requestBody.data.Gameresult.node;
  return {
    id: gameresult.id,
    time: gameresult.time,
    tournament: gameresult.tournament.id,
    bggid: gameresult.bggInfo.bggid.toString(),
    scores: gameresult.scores.map(s => ({ player: s.player.id, score: s.score }))
  };
}
