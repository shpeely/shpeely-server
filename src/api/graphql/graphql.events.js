import logger from 'winston';

const EventEmitter = require('events');

function createListener(event, callback) {
  return (tournamentId, gameResultId) => callback(event, tournamentId, gameResultId);
}


export const emitter = new EventEmitter();

export const events = {
  gameResultUpdated: 'game-result-updated',
  gameResultDeleted: 'game-result-deleted'
}

/**
 * Emitted when a game result has been added or updated and the game-stats component finished computing the stats.
 * @param tournamentId The id of the tournament the game result was updated in
 * @param gameResultId The id of the updated / new game result
 */
export function emitGameResultUpdated(tournamentId, gameResultId) {
  const payload = {
    event: events.gameResultUpdated,
    tournamentId,
    gameResultId
  };

  emitter.emit('*', payload);
  emitter.emit(events.gameResultUpdated, payload);
  emitter.emit(`${tournamentId}:${events.gameResultUpdated}`, payload);
  logger.debug('Emitted event', `${tournamentId}:${events.gameResultUpdated}`, payload)
}

export function emitGameResultDeleted(tournamentId, gameResultId) {
  const payload = {
    event: events.gameResultDeleted,
    tournamentId,
    gameResultId
  };

  emitter.emit('*', payload);
  emitter.emit(events.gameResultDeleted, payload);
  emitter.emit(`${tournamentId}:${events.gameResultDeleted}`, payload);
  logger.debug('Emitted event', `${tournamentId}:${events.gameResultDeleted}`, payload)
}

export function addStatsListener(callback) {
  Object.keys(events).map(k => events[k]).forEach((event) => {
    emitter.addListener(event, callback);
  });
}

export function addTournamentListener(tournamentId, callback) {
  Object.keys(events).map(k => events[k]).forEach((event) => {
    emitter.addListener(`${tournamentId}:${event}`, callback);
  });
}

export function removeTournamentListener(tournamentId, callback) {
  Object.keys(events).map(k => events[k]).forEach((event) => {
    emitter.removeListener(`${tournamentId}:${event}`, createListener(event, callback))
  });
}
