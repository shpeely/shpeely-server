import request from 'supertest';
import app from '../..';

import newGameResult from './fixtures/newGameResult4.json';

describe('Statistics API:', function () {

  this.timeout(30 * 1000)

  describe('POST /api/statistics', () => {
    it('should graphql with success', (done) => {
      request(app)
        .post('/api/graphql/')
        .send(newGameResult)
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          console.log(res.body);
          if (err) {
            return done(err);
          }
          done();
        });
    });
  });
});
