import logger from 'winston';
import { addStatsListener } from './graphql.events';

export function register(socket) {
  socket.on('subscribe', (data) => {
    logger.debug(`Client ${socket.id} subscribed to ${data.tournament}`);
    socket.join(data.tournament);
  })
  socket.on('unsubscribe', (data) => socket.leave(data.tournament))
}

export function registerChannel(socketio) {
  addStatsListener(({event, tournamentId, gameResultId}) => {
    logger.debug('Emitting event ', event, {tournamentId, gameResultId})
    socketio.sockets.in(tournamentId).emit(event, {tournamentId, gameResultId});
  })
}

export default {
  register,
  registerChannel,
}

