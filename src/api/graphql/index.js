const express = require('express');
const controller = require('./graphql.controller');

const router = express.Router();

router.post('/', controller.handleRequest);

module.exports = router;
