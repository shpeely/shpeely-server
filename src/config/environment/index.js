/* eslint no-process-env:0*/

import fs from 'fs';
import path from 'path';
import _ from 'lodash';
import moment from 'moment';

function getArray(envVariable) {
  return envVariable
    ? envVariable.split(',')
    : null;
}

// All configurations will extend these options
// ============================================

const all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(`${__dirname}/../../..`),

  // Server port
  port: process.env.PORT || 8000,

  // Server IP
  ip: process.env.IP || '0.0.0.0',

  // Should we populate the DB with sample data?
  seedDB: false,

  // Should we run the startup tasks?
  runStartupTasks: true,

  // enabled startup tasks
  enableStartupTasks: getArray(process.env.ENABLE_STARTUP_JOBS) || [],

  // Should we run the cron jobs?
  runCronJobs: false,

  // Secret for session
  secrets: {
    session: 'shpeely-server-secret',
  },

  // logging configuration
  // see (https://github.com/winstonjs/winston/blob/master/docs/transports.md#console-transport)
  logging: {
    level: 'debug',
    colorize: true,
    timestamp: () => moment().format('YY.MM.DD - HH:mm:ss')
  },

  // MongoDB connection options
  mongo: {
    options: {
      db: {
        safe: true,
      },
    },
  },

  gameStats: {
    uri: process.env.GAME_STATS_URI
  },

  graphCool: {
    uri: process.env.GRAPHCOOL_URI,
    token: process.env.GRAPHCOOL_TOKEN
  },

  legacyShpeely: {
    uri: 'https://shpeely.com/api',
  },

  bggMirror: process.env.BGG_MIRROR,

  facebook: {
    clientID: process.env.FACEBOOK_ID || 'id',
    clientSecret: process.env.FACEBOOK_SECRET || 'secret',
    callbackURL: `${process.env.DOMAIN || ''}/auth/facebook/callback`,
  },

  twitter: {
    clientID: process.env.TWITTER_ID || 'id',
    clientSecret: process.env.TWITTER_SECRET || 'secret',
    callbackURL: `${process.env.DOMAIN || ''}/auth/twitter/callback`,
  },

  google: {
    clientID: process.env.GOOGLE_ID || 'id',
    clientSecret: process.env.GOOGLE_SECRET || 'secret',
    callbackURL: `${process.env.DOMAIN || ''}/auth/google/callback`,
  },
};

// load local configuration if local.env exists
let localConfig = {};
if (fs.existsSync('../local.env.js')) {
  localConfig = require('../local.env');
}

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./shared'),
  // eslint-disable-next-line import/no-dynamic-require, global-require
  require(`./${process.env.NODE_ENV || 'development'}.js`) || {},
  localConfig
  );
