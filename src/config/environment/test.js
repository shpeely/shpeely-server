

/* eslint no-process-env:0*/

// Test specific configuration
// ===========================
module.exports = {
  // MongoDB connection options
  mongo: {
    uri: process.env.MONGODB_URI || 'mongodb://localhost/shpeelyserver-test',
  },

  gameStats: {
    uri: process.env.GAME_STATS_URI || 'http://localhost:5000'
  },

  graphCool: {
    uri: 'https://api.graph.cool/simple/v1/shpeely-dev'
  },
};
