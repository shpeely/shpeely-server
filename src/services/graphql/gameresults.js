import logger from 'winston';
import graphQLClient from './index'

const gameResultsQuery = `
  query deleteGameresult($gameresultId: ID!) {
      Gameresult (
        id: $gameresultId
      ) {
      scores {
        id
      }
    }
  }
`;

const deleteScoresMutation = `
  mutation deleteScore($scoreId: ID!) {
      deleteScore (
        id: $scoreId
      ) {
      id
    }
  }
`;

const deleteGameresultMutation = `
  mutation deleteGameresult($gameresultId: ID!) {
      deleteGameresult (
        id: $gameresultId
      ) {
      id
    }
  }
`;

/**
 * Deletes all game results with the 'deleted' flag set to true. This function will also delete all related scores.
 * @param gameresultId
 * @returns {Promise.<void>}
 */
export async function deleteGameresult(gameresultId) {
  try {
    logger.debug(`Deleting game result ${gameresultId}`);
    const { Gameresult: { scores } } = await graphQLClient.request(gameResultsQuery, { gameresultId });

    logger.debug(`Deleting scores of game result ${gameresultId}`);
    for (let score of scores) {
      await graphQLClient.request(deleteScoresMutation, { scoreId: score.id });
    }
    logger.debug(`All scores of gameresult ${gameresultId} have been deleted`);
    return await graphQLClient.request(deleteGameresultMutation, { gameresultId });

  } catch (err) {
    logger.warn(`Failed to delete gameresult ${gameresultId}`, err);
  }
}
