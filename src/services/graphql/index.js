import {GraphQLClient} from 'graphql-request'
import config from '../../config/environment';
import logger from 'winston';

import {
  createBggInfo,
  upsertBggInfo,
  getBggInfoByBggId

} from './bgg-info'

import {
  deleteGameresult
} from './gameresults';

logger.info(`Using GraphQL url ${config.graphCool.uri}`);

const graphQLCLient = new GraphQLClient(config.graphCool.uri, {
  headers: {
    Authorization: `Bearer ${config.graphCool.token}`,
  },
});

export {
  createBggInfo,
  upsertBggInfo,
  getBggInfoByBggId,
  deleteGameresult
}

export default graphQLCLient
