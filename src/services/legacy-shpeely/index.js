import fetch from 'node-fetch';
import config from '../../config/environment';
import https from 'https';
import logger from 'winston';

const agent = new https.Agent({ rejectUnauthorized: false });

export default function (endpoint, options) {
  logger.debug('Request to legacy shpeely: ', `${config.legacyShpeely.uri}${endpoint}`)
  return fetch(`${config.legacyShpeely.uri}${endpoint}`, {
    agent,
    headers: { 'Content-Type': 'application/json' },
    ...options
  })
    .then(res => res.json())
}
