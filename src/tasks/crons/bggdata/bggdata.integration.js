import app from '../../..';
import { createPlain, queryPlain, destroyAllPlain } from '../../../api/bgg/bgg.controller';
import bggdata from './index';

describe('BGG Data Cron Job:', function() {

  this.timeout(30000);

  describe('Update BGG Data', () => {

    const olderGame = {
      id: 'graphqlid1',
      bggid: 31260,
      weight: 3.3,
      maxPlayers: 7.7,
      name: 'Agricola',
      timestamp: {
        updatedAt: Date.parse('01/01/2017')
      }
    };
    const newerGame = {
      id: 'graphqlid2',
      bggid: 25613,
      weight: 3.3,
      maxPlayers: 7.7,
      name: 'Through the Ages',
      timestamp: {
        updatedAt: Date.parse('01/02/2017')
      }
    };

    beforeEach(() => {
      return Promise.all([
        [olderGame, newerGame].map(createPlain)
      ]);
    });

    afterEach(() => {
      return destroyAllPlain();
    });

    it('Should get the oldest bgg document and update it.', (done) => {
      bggdata.task()

        // check that older game was updated
        .then(() => queryPlain({ bggid: olderGame.bggid }))
        .then((games) => {
          expect(games[0].yearPublished).not.to.be.undefined;
        })

        // check that newer game was not updated
        .then(() => queryPlain({ bggid: newerGame.bggid }))
        .then((games) => {
          expect(games[0].yearPublished).to.be.undefined;
        })

        .then(() => done())
        .catch(done);
    });

  });
});
