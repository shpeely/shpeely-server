import cron from 'node-cron';
import logger from 'winston';
import config from '../config/environment';

// cron jobs
import bggdata from './crons/bggdata';

// startup tasks
import bggDataSeed from './startup/bgg-data-seed';
import graphQlSync from './startup/graphcool-sync';
import legacyShpeelySync from './startup/legacy-shpeely-sync';

const crons = [
  bggdata
];

const tasks = [
  bggDataSeed,
  graphQlSync,
  legacyShpeelySync
];

async function start() {
  if (config.runCronJobs) {
    logger.debug('Scheduling cron tasks...');
    crons.forEach(job => cron.schedule(job.interval, job.task, job.immediate).start());
  }

  if (config.runStartupTasks && config.enableStartupTasks.length) {
    logger.debug(`Running startup task(s): ${config.enableStartupTasks.join(', ')}`);
    for (let task of tasks) {

      if (!config.enableStartupTasks.includes(task.name)) {
        logger.debug(`Startup job ${task.name} is not enabled.`);
      } else {
        logger.debug(`Running startup job ${task.name}`);

        try {
          await task.start();
        } catch (err) {
          logger.error('Startup job failed: ', err);
        }
      }
    }
    logger.info('Startup jobs finished.')
  } else {
    logger.info('Startup jobs are disabled.');
  }
}

export default { start };
