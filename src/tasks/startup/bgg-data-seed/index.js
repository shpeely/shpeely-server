// Startup job to fetch BGG data

import logger from 'winston';

import '../../..'; // import of the app
import { syncPlain } from '../../../api/bgg/bgg.controller';
import bggids from './bggids';

export const JOB_NAME = 'bgg-data-seed';

const REQUEST_INTERVAL = 10 * 1000; // 10s

/**
 * The task to execute
 */
function start(idsToLoad) {

  logger.debug(`Run ${JOB_NAME} startup task.`);

  const ids = idsToLoad || bggids;
  if (!ids.length) { return Promise.resolve(); }

  return new Promise((resolve) => {

    // syncing function
    function sync(index) {
      if (index < ids.length) {
        syncPlain(ids[index])
          .then(doc => logger.debug('Downloaded %s data from BGG', doc.name))
          .catch(err => logger.error('Failed to fetch data from BGG', err));
        setTimeout(() => sync(index + 1), REQUEST_INTERVAL);
      } else {
        resolve();
      }
    }

    // start the syncing
    sync(0);
  });
}

export default {
  start,
  name: JOB_NAME
};
