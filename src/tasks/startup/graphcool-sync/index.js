// Startup job to synchronize game-stats with GraphCool

import logger from 'winston';
import _ from 'lodash';
import fetch from 'node-fetch';
import config from '../../../config/environment';
import {syncPlain as getBggData} from '../../../api/bgg/bgg.controller';
import graphQlClient from '../../../services/graphql';

export const JOB_NAME = 'graphcool-sync';

const DIFF = {
  missing: 'missing',
  deleted: 'deleted',
  changed: 'changed',
  inSync: 'inSync',
};

function transformGameResult(tournament, gameresult) {
  return {
    tournament,
    time: gameresult.time,
    id: gameresult.id,
    bggid: gameresult.bggInfo.bggid.toString(),
    scores: gameresult.scores.map(s => ({player: s.player.id, score: s.score}))
  };
}

const tournamentsQuery = `
  query tournaments {
    tournaments: allTournaments {
      id
    }
  }
`

const gameresultsQuery = `
  query gameresults($tournamentId: ID!) {
    gameresults: allGameresults(
      filter: {
        tournament: {
          id: $tournamentId
        }
        deleted: false
      }
      orderBy: time_ASC
    ) {
      id
      time
      bggInfo {
        bggid
      }
      createdAt
      scores {
        player {
          id
        }
        score
      }
    }
  }
`

function isInSync(graphQlResult, gameStatsResult) {
  const graphQlScores = graphQlResult.scores.map(s => ({player: s.player.id, score: s.score}))
  const scoresInSync = _.isEqual(_.sortBy(graphQlScores, 'player'), _.sortBy(gameStatsResult.scores, 'player'));
  const gameIdInSync = graphQlResult.bggInfo.bggid.toString() === gameStatsResult.game_id;
  return scoresInSync && gameIdInSync;
}

async function getWeight(bggid) {
  const gameInfo = await getBggData(bggid);
  return gameInfo.weight;
}

/**
 * The task to execute
 */
async function start() {
  logger.debug(`Starting ${JOB_NAME}`);

  const tournamnentsGraphQL = await graphQlClient.request(tournamentsQuery);
  const tournamentIds = _.map(tournamnentsGraphQL.tournaments, 'id');

  for (let tournamentId of tournamentIds) {
    logger.debug(`Sync tournament ${tournamentId}`);

    // load data from graphql
    let resultsGraphQL;
    resultsGraphQL = await graphQlClient.request(gameresultsQuery, {tournamentId});

    // load data from game stats
    let resultsGameStats;
    resultsGameStats = await fetch(`${config.gameStats.uri}/tournament/${tournamentId}/gameresult`)
      .then(res => res.json());

    const resultsGraphQLIds = _.map(resultsGraphQL.gameresults, 'id');
    const resultsGameStatsIds = _.map(resultsGameStats, 'id');

    const deletedResults = _.difference(resultsGameStatsIds, resultsGraphQLIds);

    const diff = _.groupBy(resultsGraphQL.gameresults, (result) => {
      if (!resultsGameStatsIds.includes(result.id)) {
        return DIFF.missing;
      } else if (!isInSync(result, resultsGameStats.find(r => r.id === result.id))) {
        return DIFF.changed;
      }
      return DIFF.inSync;
    });

    diff[DIFF.deleted] = deletedResults;
    diff[DIFF.changed] = diff[DIFF.changed] || [];
    diff[DIFF.missing] = diff[DIFF.missing] || [];
    diff[DIFF.inSync] = diff[DIFF.inSync] || [];

    logger.debug(`Sync result of tournament ${tournamentId}:
      ${resultsGameStatsIds.length} games in game-stats
      ${resultsGraphQLIds.length} games in graphql
      ${diff[DIFF.missing].length} missing,
      ${diff[DIFF.deleted].length} deleted,
      ${diff[DIFF.changed].length} changed,
      ${diff[DIFF.inSync].length} in sync.`
    );

    // synchronize game-stats

    // delete
    for (let deleted of diff[DIFF.deleted]) {
      await fetch(`${config.gameStats.uri}/tournament/${tournamentId}/gameresult/${deleted}`, {
        method: 'DELETE'
      });
    }

    // add / update
    for (let result of diff[DIFF.missing].concat(diff[DIFF.changed])) {
      const gameWeight = await getWeight(result.bggInfo.bggid);

      // save the game weight
      await fetch(`${config.gameStats.uri}/game/${result.bggInfo.bggid}/weight/${gameWeight}`, {
        method: 'POST'
      });

      // save the game result
      await fetch(`${config.gameStats.uri}/tournament/${tournamentId}/gameresult`, {
        method: 'POST',
        body: JSON.stringify(transformGameResult(tournamentId, result)),
        headers: {'Content-Type': 'application/json'}
      });
    }
  }

  logger.info(`Task ${JOB_NAME} is complete.`);
}

export default {
  start,
  name: JOB_NAME
};
